import unittest
from ddt import ddt, data, unpack

def plakatuj_rekursywnie_wrapper(ys: list):
    odp = 0
    # plakatuj_rekursywnie wybuchnie jak dasz mu tylko 0
    if sum(ys)>0:
        odp = plakatuj_rekursywnie(ys)
    return odp

def plakatuj_kwadratowo(ys: list):
    res = 0
    tmp = []
    previous = 0
    special_case = False
    for i in ys:
        if special_case:
            special_case = False
            res += 1
        elif i not in tmp:
            tmp.append(i)
            res += 1
        elif i > previous:
            special_case = True
            res += 1
        previous = i
    return res

def plakatuj_rekursywnie(ys: list):
    odp = 0
    m = min(ys)

    if len(ys) == 1:
        return 1
    # jeżeli jest więcej elementów i nie ma w nich 0
    elif m != 0:
        # usuń od każdego min(ys) - postaw plakat w najniższym możliwym miejscu
        ys = [ y - m if y>0 else 0 for y in ys ]
        odp = 1
    # else:
    for y in list_split(ys, 0):
        odp += plakatuj_rekursywnie(y)
    return odp

def list_split(l: list, delimiter: int):
    x=[]
    for i in [ j for j in ''.join(str(v) for v in l).split(str(delimiter)) ]:
        if i != '':
            x.append([int(j) for j in list(i)])
    return x

def plakatuj_liniowo(ys: list):
    res = 1
    min = ys[0]
    local_min = ys[0]
    add_on_next_iter = False
    for i in ys:
        local_min_copy = local_min
        if add_on_next_iter:
            local_min = i
            add_on_next_iter = False
        if i < min:
            min = i
            res += 1
        elif i > min:
            if i < local_min_copy:
                local_min = i
                res += 1
            elif i > local_min_copy:
                res += 1
        else:
            add_on_next_iter = True
    return res

import random
random.seed(1337)
wrong_ys = []
for i in range(1, 20):
    ys = [random.randint(1, 20) for _ in range(i)]
    rys = plakatuj_rekursywnie_wrapper(ys)
    kys = plakatuj_kwadratowo(ys)
    lys = plakatuj_liniowo(ys)
    if rys != kys != lys:
        wrong_ys.append(ys)

for ys in wrong_ys:
    print(f"{ys}: ")
    print(f"recursive = {plakatuj_rekursywnie_wrapper(ys)}")
    print(f"O(n²) = {plakatuj_kwadratowo(ys)}")
    print(f"liniowo = {plakatuj_liniowo(ys)}")
    print()
